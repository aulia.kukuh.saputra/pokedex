import React, { createContext, useReducer } from "react";
import InventoryReducer from '../reducers/InventoryReducer';

export const InventoryContext = createContext();
export const InventoryAction = {
    ADD_POKEMON: "ADD_POKEMON",
    REMOVE_POKEMON: "REMOVE_POKEMON"
}

const InventoryContextProvider = (props) => {
    const [inventory, dispatch] = useReducer(InventoryReducer, localStorage.getItem('inventory') ? JSON.parse(localStorage.getItem('inventory')) : [])

    return (
        <InventoryContext.Provider value={{inventory, dispatch}}>
            { props.children }
        </InventoryContext.Provider>
    )
}

export default InventoryContextProvider;