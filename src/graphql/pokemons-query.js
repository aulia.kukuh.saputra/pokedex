import gql from 'graphql-tag';

export const PokemonsQuery = {
    GET_POKEMONS: gql`
        query pokemons($limit: Int, $offset: Int) {
            pokemons(limit: $limit, offset: $offset) {
                count
                results {
                    name
                    image
                }
            }
        }      
    `,
    GET_POKEMON_DETAIL: gql`
        query pokemon($name: String!) {
            pokemon(name: $name) {
                id
                name
                height
                weight
                base_experience
                stats {
                    stat {
                        name
                    }
                    base_stat
                    effort
                }
                abilities {
                    ability {
                        name
                    }
                }
                sprites {
                    front_default
                    front_shiny
                }
                moves {
                    move {
                        name
                    }
                }
                types {
                    type {
                        name
                    }
                }
            }
        }
    `
}