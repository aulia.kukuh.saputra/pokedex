import 'bulma/css/bulma.css';
import './App.scss';
import 'animate.css';

import { lazy, Suspense } from 'react';
import { HashRouter, Redirect, Route, Switch } from 'react-router-dom';
import { Section } from 'react-bulma-components';

import BottomNavigation from './components/navigations/BottomNavigation';
import TopNavigation from './components/navigations/TopNavigation';

import { ApolloProvider } from '@apollo/react-hooks';
import ApolloClient from 'apollo-boost';

const PokemonList = lazy(() => import('./components/PokemonList'));
const PokemonDetail = lazy(() => import('./components/PokemonDetail'));
const PokemonInventory = lazy(() => import('./components/PokemonInventory'));

export default function App() {
  const client = new ApolloClient({
    uri: 'https://graphql-pokeapi.vercel.app/api/graphql'
  })
  
  return (
    <ApolloProvider client={client}>
      <HashRouter>
        <TopNavigation></TopNavigation>
          <Suspense fallback={<div className="pageloader is-link is-active"><span className="title">Loading</span></div>}>
            <Section className="py-5">
              <Switch>
                <Route exact path="/" component={PokemonList}></Route>
                <Route path="/pokemon/:pokemonName" component={PokemonDetail}></Route>
                <Route path="/inventory" component={PokemonInventory}></Route>
                <Route path="*">
                  <Redirect to="/" />
                </Route>
              </Switch>
            </Section> 
          </Suspense>
        <BottomNavigation></BottomNavigation>
      </HashRouter>
    </ApolloProvider>
  )
}
