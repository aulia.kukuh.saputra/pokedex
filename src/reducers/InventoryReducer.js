import { v4 as uuidv4 } from 'uuid';

const InventoryReducer = (state, action) => {
    switch(action.type) {
        case 'ADD_POKEMON':
            state = [...state, {
                ...action.pokemon,
                nickname: action.nickname,
                isShiny: action.isShiny,
                uuid: uuidv4()
            }]
            localStorage.setItem('inventory', JSON.stringify(state))
            return state
        case 'REMOVE_POKEMON':
            state = state.filter(pokemon => pokemon.nickname !== action.nickname)
            localStorage.setItem('inventory', JSON.stringify(state))
            return state
        default:
            return state
    }
}

export default InventoryReducer;