import React, { useContext, useEffect, useState } from 'react';
import { Pagination, Container, Hero, Heading, Card, Columns } from 'react-bulma-components';
import { useLazyQuery } from '@apollo/react-hooks';
import { NavLink } from 'react-router-dom';

import { InventoryContext } from '../contexts/InventoryContext';
import { PokemonsQuery } from '../graphql/pokemons-query';

import { css } from '@emotion/react';

export default function PokemonList() {
    const [page, setPage] = useState(1)
    const [pokemonPerPage] = useState(12)
    const { inventory } = useContext(InventoryContext)

    const [getPokemons, { loading, error, data: { pokemons = {} } = {} }] = useLazyQuery(PokemonsQuery.GET_POKEMONS)
    
    const getPokemonCount = (name) => inventory.filter(pokemon => pokemon.name === name).length

    useEffect(() => {
        window.scroll({
            top: 0, 
            behavior: 'smooth'
        })
        return getPokemons({
            variables: {
                limit: pokemonPerPage,
                offset: 1 + ((page - 1) * (pokemonPerPage - 1))
            }
        })
    }, [page, pokemonPerPage, getPokemons])
    
    return (
        <Container>
            {loading && <>
                <Hero size="large">
                    <Hero.Body>
                        <img src="/assets/image/iconfinder__Time_1337508.png" css={css`margin: 0 auto;`} className="is-block" width="128" height="128" alt="Loading Icon"/>
                        <Heading size={4} className="has-text-centered">
                            Loading Pokemon List
                        </Heading>
                    </Hero.Body>
                </Hero>
            </>}
            {error && <>
                <Hero size="large">
                    <Hero.Body>
                        <img src="/assets/image/iconfinder__hatching_egg_1337478.png" css={css`margin: 0 auto;`} className="is-block" width="128" height="128" alt="Failed to Load Icon"/>
                        <Heading size={4} className="has-text-centered">
                            Failed to Load Pokemon List
                        </Heading>
                    </Hero.Body>
                </Hero>
            </>}
            {Object.keys(pokemons).length !== 0 && <>
                <nav className="breadcrumb is-medium has-text-right" aria-label="breadcrumbs" css={css`margin-bottom: .75rem !important;`}>
                    Page {page} / {Math.ceil(pokemons.count / pokemonPerPage)}
                </nav>

                <Columns className="is-mobile m-0 mb-4">
                    {pokemons.results.map((pokemon) => 
                        <Columns.Column className="is-half-mobile is-one-third-tablet is-one-quarter-desktop p-1" key={pokemon.name}>
                            <NavLink to={`/pokemon/${pokemon.name}`}>
                                <Card className="has-ribbon">
                                    <div className="card-image pt-4">
                                        <figure className="image is-96x96" css={css`margin: 0 auto;`}>
                                            <img src={pokemon.image} width="96" height="96" alt={`${pokemon.name} Sprite`}/>
                                        </figure>
                                    </div>
                                    <div className={`ribbon is-small is-${getPokemonCount(pokemon.name) === 0 ? `danger` : `primary`}`}>{getPokemonCount(pokemon.name)} Owned</div>
                                    <Card.Header className="is-shadowless" css={css`border-top: 1px solid #ededed;`}>
                                        <Card.Header.Title className="is-justify-content-center	is-capitalized">{pokemon.name}</Card.Header.Title>
                                    </Card.Header>
                                </Card>
                            </NavLink>
                        </Columns.Column>
                    )}
                </Columns>
                <Pagination current={page} total={Math.ceil(pokemons.count / pokemonPerPage)} delta={0} className="is-rounded m-0" onChange={value => setPage(value)}/>
            </>}
        </Container>
    );
}