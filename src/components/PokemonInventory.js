import React, { useContext, useEffect, useState } from 'react';
import { Pagination, Container, Hero, Heading, Columns, Card } from 'react-bulma-components';
import { NavLink } from 'react-router-dom';
import { css } from '@emotion/react';
import Swal from 'sweetalert2/src/sweetalert2.js';
import withReactContent from 'sweetalert2-react-content';

import { InventoryAction, InventoryContext } from '../contexts/InventoryContext';

export default function PokemonInventory() {
    const swal = withReactContent(Swal)
    
    const [page, setPage] = useState(1)
    const [pokemonPerPage] = useState(12)
    const { inventory, dispatch } = useContext(InventoryContext)
    const [pokemons, setPokemons] = useState([])

    const divAsLink = css`
        color: #3273dc;
        cursor: pointer;
        &:hover {
            color: #363636;
        }    
    `

    const confirmRelease = (pokemon) => swal.fire({
        title: "Confirmation",
        text: "Are you sure you want to release this pokemon?",
        confirmButtonText: "Yes",
        cancelButtonText: "No",
        showCancelButton: true,
    }).then((result) => {
        if(!result.isConfirmed) return
        dispatch({type: InventoryAction.REMOVE_POKEMON, nickname: pokemon.nickname})
    })

    useEffect(() => {
        setPokemons(inventory.slice((page - 1) * pokemonPerPage, page * pokemonPerPage))
    }, [inventory, page, pokemonPerPage])

    useEffect(() => {
        window.scroll({
            top: 0, 
            behavior: 'smooth'
        })
    }, [page])

    return (
        <Container>
            {inventory.length === 0 && 
                <Hero size="large">
                    <Hero.Body>
                        <img src="/assets/image/iconfinder__snorlax_1337513.png" css={css`margin: 0 auto;`} className="is-block" width="128" height="128" alt="Inventory Empty Icon"/>
                        <Heading size={4} className="has-text-centered">
                            Gotta Catch 'em all
                        </Heading>
                    </Hero.Body>
                </Hero>
            }
            {inventory.length > 0 && <>
                <nav className="breadcrumb is-medium has-text-right" aria-label="breadcrumbs" css={css`margin-bottom: .75rem !important;`}>
                    Page {page} / {Math.ceil(inventory.length / pokemonPerPage)}
                </nav>
                <Columns className="is-mobile m-0 mb-4">
                    {pokemons.map((pokemon) =>
                        <Columns.Column className="is-full-mobile is-one-third-tablet is-one-quarter-desktop p-1" key={pokemon.uuid}>
                            <Card>
                                <Card.Header className="is-shadowless" css={css`border-bottom: 1px solid #ededed;`}>
                                    <Card.Header.Title className="is-justify-content-center	is-capitalized">
                                        {/* <div class="media-content has-text-centered">
                                            <p class="title is-5">{pokemon.name}</p>
                                            <p class="subtitle is-6">{pokemon.nickname}</p>
                                        </div> */}
                                        {pokemon.nickname}
                                    </Card.Header.Title>
                                </Card.Header>
                                <div className="card-image pt-4">
                                    <figure className="image is-96x96" css={css`margin: 0 auto;`}>
                                        <img src={pokemon.isShiny ? pokemon.sprites.front_shiny : pokemon.sprites.front_default} width="96" height="96" alt={`${pokemon.name} Sprite`}/>
                                    </figure>
                                </div>
                                <Card.Footer>
                                    <NavLink to={`/pokemon/${pokemon.name}`} className="card-footer-item">Detail</NavLink>
                                    <Card.Footer.Item renderAs="div" css={divAsLink} onClick={() => confirmRelease(pokemon)}>Release</Card.Footer.Item>
                                </Card.Footer>
                            </Card>
                        </Columns.Column>
                    )}
                </Columns>
                <Pagination current={page} total={Math.ceil(inventory.length / pokemonPerPage)} delta={0} className="is-rounded m-0" onChange={value => setPage(value)}/>
            </>}
        </Container>
    );
}