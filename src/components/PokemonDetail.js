import React, { useEffect, useContext, useState } from 'react';
import { Container, Hero, Heading, Card, Button, Tag, Columns, Pagination } from 'react-bulma-components';
import Swal from 'sweetalert2/src/sweetalert2.js';
import withReactContent from 'sweetalert2-react-content';

import { PokemonsQuery } from '../graphql/pokemons-query';
import { useLazyQuery } from '@apollo/react-hooks';
import { InventoryContext } from "../contexts/InventoryContext";

import { css } from '@emotion/react';
import Chance from 'chance';
import { pokeball, pokeballButton } from './styled/pokeball';
import PokemonCaptured from './PokemonCaptured';

export default function PokemonDetail(props) {
    const chance = new Chance()
    const swal = withReactContent(Swal)

    const [page, setPage] = useState(1)
    const [movePerPage] = useState(14)
    const [moves, setMoves] = useState([])

    const pokemonName = props.match.params.pokemonName

    const { inventory, dispatch } = useContext(InventoryContext)

    const aboveBottomNav = css`
        position: fixed;
        bottom: 52px;
        left: 50%;
        transform: translateX(-50%);
    `
    
    const [getPokemonDetail, { loading, error, data: { pokemon = {} } = {} }] = useLazyQuery(PokemonsQuery.GET_POKEMON_DETAIL)
    
    const showAlert = () => swal.fire({
        allowEscapeKey: false,
        allowOutsideClick: false,
        showConfirmButton: false,
        width: 250,
        padding: '1em',
        html: <>
            <div css={[pokeball, css`margin: 0 auto;`]}>
                <div css={pokeballButton}></div>
            </div>
            <Heading size={4} className="is-capitalized">Catching {pokemonName}!</Heading>
        </>,
        showClass: {
            popup: 'animate__animated animate__fadeIn'
        },
        hideClass: {
            popup: 'animate__animated animate__fadeOut'
        },
        backdrop: true,
        timer: 4500
    })
    .then(() => chance.bool({likelihood: 50}))
    .then((captured) => {
        if(captured) return pokemonCapturedAlert()
        return pokemonNotCapturedAlert()
    })
    
    const pokemonCapturedAlert = () => swal.fire({
        allowEscapeKey: false,
        allowOutsideClick: false,
        showConfirmButton: false,
        padding: '1em',
        html: <>
            <PokemonCaptured pokemon={pokemon} inventory={inventory} dispatch={dispatch} close={swal.close} isShiny={chance.bool({likelihood: 20})}></PokemonCaptured>
        </>,
        showClass: {
            popup: 'animate__animated animate__fadeIn'
        },
        hideClass: {
            popup: 'animate__animated animate__fadeOut'
        },
    })

    const pokemonNotCapturedAlert = () => swal.fire({
        html: <>
            <img src="/assets/image/iconfinder__Swarm_1337510.png" width="128" height="128" alt="Pokemon Fled" />
            <Heading size={5} className="is-capitalized">Aww, the pokemon fled!</Heading>
            <Heading size={5} subtitle className="is-capitalized">Better luck next time</Heading>
        </>
    })

    useEffect(() => {
        getPokemonDetail({variables: { name: pokemonName }})
    }, [pokemonName, getPokemonDetail])

    useEffect(() => {
        if(Object.keys(pokemon).length > 0 && pokemon.id) setMoves(pokemon.moves.slice((page - 1) * movePerPage, page * movePerPage))
    }, [pokemon, page, movePerPage])

    useEffect(() => {
        window.scroll({
            top: 730,
            behavior: "smooth"
        })
    }, [page])

    return (
        <Container>
            {loading && <>
                <Hero size="large">
                    <Hero.Body>
                        <img src="/assets/image/iconfinder__Time_1337508.png" css={css`margin: 0 auto;`} className="is-block" width="128" height="128" alt="Loading Icon"/>
                        <Heading size={4} className="has-text-centered">
                            Loading Pokemon Detail
                        </Heading>
                    </Hero.Body>
                </Hero>
            </>}
            {error && <>
                <Hero size="large">
                    <Hero.Body>
                        <img src="/assets/image/iconfinder__hatching_egg_1337478.png" css={css`margin: 0 auto;`} className="is-block" width="128" height="128" alt="Failed to Load Icon"/>
                        <Heading size={4} className="has-text-centered">
                            Failed to Pokemon Detail
                        </Heading>
                    </Hero.Body>
                </Hero>
            </>}
            {(Object.keys(pokemon).length > 0 && pokemon.id) && <>
                <Columns>
                    <Columns.Column className="is-full-mobile is-half-tablet is-one-third-desktop">
                        <Card className="has-ribbon is-capitalized" css={css`.card-footer-item{padding: 0.5em;}`}>
                            <Card.Header className="is-shadowless" css={css`border-bottom: 1px solid #ededed;`}>
                                <Card.Header.Title className="is-justify-content-center">
                                    {pokemon.name}
                                </Card.Header.Title>
                            </Card.Header>
                            <div className="card-image pt-4">
                                <figure className="image is-96x96" css={css`margin: 0 auto;`}>
                                    <img src={pokemon.sprites.front_default} width="96" height="96" alt={`${pokemon.name} Sprite`}/>
                                </figure>
                            </div>
                            <Card.Footer>
                                <Card.Footer.Item renderAs="div" className="has-text-weight-semibold">
                                    Base EXP: {pokemon.base_experience}
                                </Card.Footer.Item>
                                <Card.Footer.Item renderAs="div" className="has-text-weight-semibold">
                                    Base EVs: {pokemon.stats.map(stats => stats.effort).reduce((total, value) => total + value)}
                                </Card.Footer.Item>
                            </Card.Footer>
                            <Card.Footer>
                                <Card.Footer.Item renderAs="div" className="has-text-weight-semibold" css={css`text-transform: initial !important;`}>
                                    Height: {pokemon.height / 10} m
                                </Card.Footer.Item>
                                <Card.Footer.Item renderAs="div" className="has-text-weight-semibold" css={css`text-transform: initial !important;`}>
                                    Weight: {pokemon.weight / 10} kg
                                </Card.Footer.Item>
                            </Card.Footer>
                            <Card.Footer>
                                <Card.Footer.Item renderAs="div" className="has-text-weight-bold">
                                    Types
                                </Card.Footer.Item>
                            </Card.Footer>
                            <Card.Footer>
                                <Card.Footer.Item renderAs="div" className="has-text-weight-semibold">
                                    <Tag.Group className="are-medium">
                                        {pokemon.types.map(pokemon => 
                                            <Tag color="link" key={pokemon.type.name}>
                                                {pokemon.type.name}
                                            </Tag>
                                        )}
                                    </Tag.Group>
                                </Card.Footer.Item>
                            </Card.Footer>
                            <Card.Footer>
                                <Card.Footer.Item renderAs="div" className="has-text-weight-bold">
                                    Base Stats
                                </Card.Footer.Item>
                            </Card.Footer>
                            <Card.Footer>
                                <Card.Footer.Item renderAs="div" className="has-text-weight-semibold">
                                    {pokemon.stats[0].stat.name}: {pokemon.stats[0].base_stat}
                                </Card.Footer.Item>
                                <Card.Footer.Item renderAs="div" className="has-text-weight-semibold">
                                    {pokemon.stats[5].stat.name}: {pokemon.stats[5].base_stat}
                                </Card.Footer.Item>
                            </Card.Footer>
                            <Card.Footer>
                                {pokemon.stats.slice(1, 3).map(stats =>
                                    <Card.Footer.Item renderAs="div" className="has-text-weight-semibold" key={stats.stat.name}>
                                        {stats.stat.name}: {stats.base_stat}
                                    </Card.Footer.Item>
                                )}
                            </Card.Footer>
                            {pokemon.stats.slice(3, 5).map(stats =>
                                <Card.Footer key={stats.stat.name}>
                                    <Card.Footer.Item renderAs="div" className="has-text-weight-semibold">
                                        {stats.stat.name}: {stats.base_stat}
                                    </Card.Footer.Item>
                                </Card.Footer>
                            )}
                            <Card.Footer>
                                <Card.Footer.Item renderAs="div" className="has-text-weight-bold">
                                    Abilities
                                </Card.Footer.Item>
                            </Card.Footer>
                            <Card.Footer>
                                {pokemon.abilities.map(abilities => 
                                    <Card.Footer.Item renderAs="div" className="has-text-weight-semibold" key={abilities.ability.name}>
                                        {abilities.ability.name}
                                    </Card.Footer.Item>
                                )}
                            </Card.Footer>
                        </Card>
                    </Columns.Column>
                    <Columns.Column className="is-full-mobile is-half-tablet is-two-thirds-desktop">
                        <Card className="has-ribbon is-capitalized" css={css`.card-footer-item{padding: 0.5em;}`}>
                            <Card.Header className="is-shadowless">
                                <Card.Header.Title className="is-justify-content-center">
                                    Move
                                </Card.Header.Title>
                            </Card.Header>
                            {moves.map(moves => 
                                <Card.Footer key={moves.move.name}>
                                    <Card.Footer.Item renderAs="div" className="has-text-weight-semibold">
                                        {moves.move.name}
                                    </Card.Footer.Item>
                                </Card.Footer>
                            )}
                        </Card>
                    </Columns.Column>
                </Columns>
                <Pagination current={page} total={Math.ceil(pokemon.moves.length / movePerPage)} delta={0} className="is-rounded m-0 mb-6 is-hidden-tablet" onChange={value => setPage(value)}/>
                <Pagination current={page} total={Math.ceil(pokemon.moves.length / movePerPage)} delta={0} className="is-rounded m-0 mb-5 is-hidden-mobile" onChange={value => setPage(value)}/>
                <Button color="warning" css={aboveBottomNav} className="has-text-black is-medium is-fullwidth is-capitalized has-text-weight-semibold" onClick={showAlert}>Catch {pokemonName}</Button>
            </>}
            {(Object.keys(pokemon).length > 0 && pokemon.id === null) && <>
                <Hero size="large">
                    <Hero.Body>
                        <img src="/assets/image/iconfinder__hatching_egg_1337478.png" css={css`margin: 0 auto;`} className="is-block" width="128" height="128" alt="Failed to Load Icon"/>
                        <Heading size={4} className="has-text-centered is-capitalized">
                            pokemon <code>{pokemonName}</code> not found 
                        </Heading>
                    </Hero.Body>
                </Hero>
            </>}
        </Container>
    );
}