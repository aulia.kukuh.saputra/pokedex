import { css } from '@emotion/react';
import { Navbar } from 'react-bulma-components';
import { NavLink } from 'react-router-dom';

export default function BottomNavigation() {
    return (
        <Navbar fixed="bottom" className="level is-link is-mobile is-align-items-stretch">
            <NavLink 
                exact 
                to="/" 
                css={css`
                    width: 50%;
                    &.active {
                        border-bottom: 0.2rem solid white;
                    }
                `}
                className="level-item navbar-item is-flex has-text-weight-semibold has-text-centered m-0 has-background-link has-text-white"
                isActive={(match, location) => {
                    if (match) return true;
                    if (location.pathname !== "/inventory") return true;
                }}>
                Pokemon List
            </NavLink>
            <NavLink
                exact 
                to="/inventory"
                css={css`
                    width: 50%;
                    &.active {
                        border-bottom: 0.2rem solid white;
                    }
                `}
                className="level-item navbar-item is-flex has-text-weight-semibold has-text-centered m-0 has-background-link has-text-white">
                My Pokemon
            </NavLink>
        </Navbar>
    )
}