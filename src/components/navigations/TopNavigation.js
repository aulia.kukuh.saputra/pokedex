import { css } from '@emotion/react';
import { Navbar } from 'react-bulma-components';
import { NavLink } from 'react-router-dom';

export default function TopNavigation() {
    return (
        <Navbar color="link">
            <Navbar.Brand>
                <NavLink to="/" css={css`letter-spacing: .5px;&:focus { background-color: initial !important; }`} className="navbar-item has-text-weight-semibold">
                    <img src="/assets/image/iconfinder__Pokeball_1337537.png" width="28" height="28" alt="Pokedex: Gotta catch 'em all" className="mr-1"/>
                    Pokedex
                </NavLink>
            </Navbar.Brand>
        </Navbar>
    )
}