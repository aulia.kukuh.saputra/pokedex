import { useForm } from "react-hook-form";
import { Heading } from "react-bulma-components";
import { InventoryAction } from "../contexts/InventoryContext";

export default function PokemonCaptured(props) {
    const { register, errors, handleSubmit } = useForm({reValidateMode: "onSubmit"})

    const submitNickname = data => {
        props.dispatch({type: InventoryAction.ADD_POKEMON, pokemon: props.pokemon, nickname: data.nickname, isShiny: props.isShiny})
        props.close()
    }

    const isNicknameNotTaken = (data) => props.inventory.filter(pokemon => pokemon.nickname.toUpperCase() === data.toUpperCase()).length === 0

    return (
        <form onSubmit={handleSubmit(submitNickname)} method="post">
            <img src="/assets/image/iconfinder__Gotcha_1337481.png" className="animate__animated animate__tada" width="128" height="128" alt="Pokemon Catched" />
            <Heading size={5} className="is-capitalized">Congrats, you caught {props.isShiny && "Shiny"} {props.pokemon.name}!</Heading>
            <div className="field">
                <div className="control">
                    <label className="label is-capitalized has-text-left">Name your pokemon</label>
                    <input className={`input`} name="nickname" ref={register({ required: true, validate: isNicknameNotTaken })} type="text" placeholder="Nickname" autoComplete="off" />
                    {errors.nickname?.type === "required" && <p className="help is-danger has-text-left">Nickname cannot be empty</p>}
                    {errors.nickname?.type === "validate" && <p className="help is-danger has-text-left">Nickname is already taken</p>}
                </div>
            </div>
            <div className="field is-grouped is-justify-content-center">
                <p className="control">
                    <button className="button is-link" type="submit">
                        Claim
                    </button>
                </p>
                <p className="control">
                    <button className="button is-warning" type="button" onClick={props.close}>
                        Release
                    </button>
                </p>
            </div>
        </form>
    )
}